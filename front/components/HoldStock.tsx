import React from 'react'

type Props = {
    name: string,
    code: string
    quantity: number,
    currentValue: number,
    changePercentage: number
}

const HoldStock = ({name, quantity, currentValue, changePercentage, code}: Props) => {
    return (
        <div className="hold-stock-container">
            <div className="name-container">
                <label>{name}</label>
                <label className="text-light">{code}</label>
            </div>
            <label>{quantity} stocks</label>
            <div className="price-container">
                <label className={changePercentage > 0? 'success' : changePercentage < 0 ? 'error' : ''}>{changePercentage}%</label>
                <label className="text-light">${currentValue}</label>
            </div>
        </div>
    )
}

export default HoldStock