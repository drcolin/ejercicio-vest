import React from 'react'

type Props = {
    onBuy: Function,
    onSell: Function,
    errorMessage: string,
    onChangeCode: Function,
    onChangeQuantity: Function,
    code: string,
    quantity: number,
}

const BuySellCard = ({ onBuy, onSell, errorMessage, onChangeCode, onChangeQuantity, code, quantity }: Props) => {

    const disableBtns = quantity <= 0 || code.length === 0

    return (
        <div className="buy-sell-card-container">
            <div className="line"><label>Stock name</label> <input value={code} onChange={(e) => onChangeCode(e.target.value)} type="text" /></div>
            <div className="line"><label>Quantity</label> <input value={quantity} onChange={(e) => onChangeQuantity(e.target.value)} type="number" /></div>
            {
                errorMessage.length > 0 &&
                <div className="line"><label className="errors">{errorMessage}</label></div>
            }
            <div className="line"><button disabled={disableBtns} onClick={() => onBuy()} className={disableBtns ? 'buy -disabled' : 'buy'}>Buy</button><button disabled={disableBtns} onClick={() => onSell()} className={disableBtns ? 'sell -disabled' : 'sell'}>Sell</button></div>
        </div>
    )
}

export default BuySellCard