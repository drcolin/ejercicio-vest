import React from 'react'

type Props = {
	children: any
}

const Card = ({children}: Props) => {

	return (
		<div className="card">
		    {children}
		</div>
	)
}

export default Card