import React, { useEffect, useState } from 'react'
import Card from "../components/Card"
import Chart from "../components/Chart"
import Head from 'next/head'
import HoldStock from '../components/HoldStock'
import BuySellCard from '../components/BuySellCard'
import axios from 'axios'

export default function Home() {

  const [quantity, setQuantity] = useState(0)
  const [stockCode, setStockCode] = useState('')
  const [errorMessage, setErrorMessage] = useState('')
  const [stocksArray, setStocksArray] = useState([])

 const url = `${process.env.NEXT_PUBLIC_API_URL}/stocks`

  useEffect(() => {
    getStocksInfo()
  },[])

  async function buySellStocks(is_buy: boolean) {

    const data = {
      code: stockCode,
      quantity,
      is_buy,
    }

    try {
      const response = await axios.post(url, data)
      if (response.status === 200) {
        setQuantity(0)
        setStockCode('')
        setErrorMessage('')
        getStocksInfo()
      }
    } catch (error) {
      if (error && error.response && error.response.data && error.response.data.detail) {

        const errors = error.response.data.detail
        setErrorMessage(errors)
      } else {
        setErrorMessage('Unexpected error')
      }
    }
  }

  async function getStocksInfo() {

    try {
      const response = await axios.get(url)

      if (response.status === 200) {
        setStocksArray(response.data)
      }

    } catch (error) {
      
    }
  }

  return (
    <div className="main">
      <Head>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet" />
      </Head>
      <div className="card-container">
        <Card>
          <h2>Current portfolio</h2>
          {
            stocksArray.map((s: {name: string, code: string, held_shares: number, change_percentage: number, current_value: number}) => {
              return (
                <HoldStock key={s.code} code={s.code} name={s.name} changePercentage={s.change_percentage} currentValue={s.current_value} quantity={s.held_shares} />
              )
            })
          }
        </Card>
      </div>
      <div className="card-container">
        <Card>
          <h2>Buy/sell stock</h2>
          <BuySellCard
            onBuy={() => buySellStocks(true)}
            onSell={() => buySellStocks(false)}
            code={stockCode}
            onChangeCode={setStockCode}
            quantity={quantity}
            onChangeQuantity={setQuantity}
            errorMessage={errorMessage}
          />
        </Card>
      </div>
      <div>
        <Chart />
      </div>
    </div>
  )
}
