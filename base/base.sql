CREATE TABLE IF NOT EXISTS stock(
	id SERIAL PRIMARY KEY,
	code VARCHAR(10) NOT NULL,
	name VARCHAR(200) NOT NULL 
);

CREATE TABLE IF NOT EXISTS transactiontype(
	id SERIAL PRIMARY KEY,
	code VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS portfolio(
	id SERIAL PRIMARY KEY,
	id_stock INT NOT NULL,
	quantity INT NOT NULL,
	mean_value INT NOT NULL,
	FOREIGN KEY (id_stock) REFERENCES stock(id)
);

CREATE TABLE IF NOT EXISTS transactionrecord(
	id SERIAL PRIMARY KEY,
	id_stock INT NOT NULL,
	quantity INT NOT NULL,
	price FLOAT NOT NULL,
	id_transaction_type INT NOT NULL,
	datetime TIMESTAMP NOT NULL,
	FOREIGN KEY (id_stock) REFERENCES stock(id),
	FOREIGN KEY (id_transaction_type) REFERENCES transactiontype(id)
);

INSERT INTO transactiontype(code) VALUES('buy');
INSERT INTO transactiontype(code) VALUES('sell');