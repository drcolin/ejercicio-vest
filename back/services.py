import requests
from models.stock import Stock
from models.portfolio import Portfolio
from models.transaction_type import TransactionType
from fastapi_sqlalchemy import db

def get_symbol_data(code: str) -> bool:
	url = f"https://api.nasdaq.com/api/quote/{code}/info?assetclass=stocks"
	headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0"}
	response = requests.get(url, headers=headers, timeout=10)
	body = response.json()
	if response.status_code == 200 and body.get('data', None) != None:
		return body.get('data', {})
	return None

def get_create_stock(code: str, name: str) -> int:
	stock = db.session.query(Stock).filter(Stock.name == name, Stock.code == code).first()
	if not stock:
		new_stock = Stock(
			name=name,
			code=code
		)
		db.session.add(new_stock)
		db.session.commit()
		db.session.refresh(new_stock)
		return new_stock.id
	return stock.id

def get_transaction_type(code: str) -> int:
	transaction_type = db.session.query(TransactionType).filter(TransactionType.code == code).first()
	return transaction_type.id 

def had_stocks(id_stock: int, quantity_expected: int) -> bool:
	portfolio_stock = db.session.query(Portfolio).filter(Portfolio.id_stock == id_stock).first()
	if not portfolio_stock or portfolio_stock.quantity < quantity_expected:
		return False
	
	return True

def calculate_mean(old_quantity: int, transaction_quantity: int, old_mean: float, transaction_price: float, is_buy: bool) -> float:

	if is_buy:
		new_quantity = old_quantity + transaction_quantity
		new_mean = (old_mean * old_quantity + transaction_price*transaction_quantity) / new_quantity
		return new_mean
	elif old_quantity - transaction_quantity > 0:
		new_quantity = old_quantity - transaction_quantity
		new_mean = (old_mean * old_quantity - old_mean*transaction_quantity) / new_quantity
		return new_mean
	return 0