from pydantic import BaseModel, Field, conint

class TransactionSchema(BaseModel):
    code: str = Field(min_length=1, max_length=10)
    quantity: conint(gt=0)
    is_buy: bool
