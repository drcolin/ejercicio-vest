import os
from dotenv import load_dotenv
import json
from sqlalchemy.ext.declarative import declarative_base

load_dotenv()

Base = declarative_base()

DB_URL = os.environ.get('DB_URL', '')
ALLOWED_HOSTS = json.loads(os.environ.get('ALLOWED_HOSTS', '[]'))

