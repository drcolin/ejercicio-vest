from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from config import Base

class TransactionType(Base):

    __tablename__ = 'transactiontype'

    id = Column(Integer, primary_key=True)
    code = Column(String, nullable=False)
    # transaction_record = relationship('TransactionRecord')