from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from config import Base

class Stock(Base):

    __tablename__ = 'stock'

    id = Column(Integer, primary_key=True)
    code = Column(String, nullable=False)
    name = Column(String, nullable=False)
    # portfolio = relationship('Portfolio')
    # transaction = relationship('Transaction')