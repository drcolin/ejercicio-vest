from sqlalchemy import Column, Integer, Float
from sqlalchemy.sql.schema import ForeignKey
from config import Base

class Portfolio(Base):

    __tablename__ = 'portfolio'

    id = Column(Integer, primary_key=True)
    id_stock = Column(Integer, ForeignKey('stock.id'), nullable=False)
    quantity = Column(Integer, nullable=False)
    mean_value = Column(Float, nullable=False)