from sqlalchemy import Column, Integer, Float, DateTime, Boolean
from sqlalchemy.sql.schema import ForeignKey
from config import Base

class TransactionRecord(Base):

    __tablename__ = 'transactionrecord'

    id = Column(Integer, primary_key=True)
    id_stock = Column(Integer, ForeignKey('stock.id'), nullable=False)
    quantity = Column(Integer, nullable=False)
    price = Column(Float, nullable=False)
    id_transaction_type = Column(Integer, ForeignKey('transactiontype.id'), nullable=False)
    datetime = Column(DateTime, nullable=False)