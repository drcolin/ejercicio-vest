import datetime

from sqlalchemy.util.langhelpers import portable_instancemethod
from schemas.transaction_schema import TransactionSchema
from fastapi import FastAPI, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi_sqlalchemy import DBSessionMiddleware
from fastapi_sqlalchemy import db

from config import ALLOWED_HOSTS, DB_URL
from services import calculate_mean, get_symbol_data, get_create_stock, get_transaction_type, had_stocks
from models.transaction import TransactionRecord
from models.transaction_type import TransactionType
from models.portfolio import Portfolio
from models.stock import Stock

app = FastAPI()

app.add_middleware(DBSessionMiddleware, db_url=DB_URL)

app.add_middleware(
    CORSMiddleware,
    allow_origins=ALLOWED_HOSTS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post('/stocks')
def buy_sell_stocks(buy_schema: TransactionSchema):

    symbol_data = get_symbol_data(buy_schema.code)

    if symbol_data != None:
        transaction_type = 'buy' if buy_schema.is_buy else 'sell'
        id_stock = get_create_stock(symbol_data.get('symbol', ''), symbol_data.get('companyName', ''))
        id_transaction_type = get_transaction_type(transaction_type)

        price = float(symbol_data.get('primaryData', {}).get('lastSalePrice', '0').replace('$',''))

        if transaction_type == 'sell' and not had_stocks(id_stock, buy_schema.quantity):
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="You don't have enought stocks")

        new_transaction = TransactionRecord(
            id_stock = id_stock,
            quantity = buy_schema.quantity,
            price = price,
            id_transaction_type = id_transaction_type,
            datetime = datetime.datetime.now()
        )
        db.session.add(new_transaction)

        portfolio_data = db.session.query(Portfolio).filter(Portfolio.id_stock == id_stock).first()

        if not portfolio_data:
            new_portfolio = Portfolio(
                id_stock = id_stock,
                quantity = buy_schema.quantity,
                mean_value = price
            )

            db.session.add(new_portfolio)
        else:
            new_quantity = portfolio_data.quantity + buy_schema.quantity if buy_schema.is_buy else portfolio_data.quantity - buy_schema.quantity
            new_mean = calculate_mean(portfolio_data.quantity, buy_schema.quantity, portfolio_data.mean_value, price, buy_schema.is_buy)
            db.session.query(Portfolio).update({
                "quantity": new_quantity,
                "mean_value": new_mean
            })

        db.session.commit()

        return {"detail": "Successfull transaction"}

    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid stock code")

@app.get('/stocks')
def get_stocks():
    portfolio = db.session.query(Portfolio, Stock.code, Stock.name).join(Stock).filter(Portfolio.quantity > 0).all()
    res = []
    for ele in portfolio:
        portfolio_data = ele.Portfolio
        symbol_data = get_symbol_data(ele.code)
        current_value = float(symbol_data.get('primaryData', {}).get('lastSalePrice', '0').replace('$',''))
        change_percentage = (current_value - portfolio_data.mean_value) * 100 / portfolio_data.mean_value
        res.append({
            'name': ele.name,
            'code': ele.code,
            'held_shares': portfolio_data.quantity,
            'change_percentage': change_percentage,
            'current_value': current_value * portfolio_data.quantity,
            # 'lowest_price': ,
            # 'highest_price': ,
            # 'avg_price': ,
        })
    return res

@app.get('/stocks/{stock_id}')
def get_stock_history():
    return {}