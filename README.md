# Ejercicio vest

Prueba tecnica de vest

## Tecnologías

- Docker

### Front

- React
- NextJS
- SASS
- Typescript

### Back

- FastAPI
- PostgreSQL
- SQLAlchemy


## Pasos para probar el proyecto

Crear el archivo .env en la carpeta del back y pegar los valores que se encuentran en la wiki del proyecto de git [LINK](https://gitlab.com/drcolin/ejercicio-vest/-/wikis/ENV-back)

Crear el archivo .env.local en la carpeta front y pegar los valores que se encuentran en la wiki del proyecto de git [LINK](https://gitlab.com/drcolin/ejercicio-vest/-/wikis/env-front)

Una vez hecho esto, en la raiz del proyecto correr el siguiente comando 

```
docker compose -f website.yml up -d
```

El puerto del proyecto en el back es el 8000 y en el front el 3000
